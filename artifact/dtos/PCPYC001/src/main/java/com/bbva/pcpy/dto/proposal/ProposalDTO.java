package com.bbva.pcpy.dto.proposal;

import java.util.Date;

import com.bbva.apx.dto.AbstractDTO;

public class ProposalDTO extends AbstractDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7137934851558701324L;
	private long idProposal;
	private String codigo;
	private String etapa;
	private Date fechaCreacion;
	private String estadoId;
	private String escenarioActual;
	private String usuarioIdCreacion;
	private String canal;
	private String bancoId;
	private String sucursalID;
	private long cantidadParticipantes;
	private long cantidadProductos;
	private long cantidadGarantia;
	private int inicialId;
	private double montoRequerido;
	private String monedaRequerida;
	private double montoAprobado;
	private String monedaAprobado;
	private double montoDesembolsado;
	private String monedaDesembolsado;

	public long getIdProposal() {
		return idProposal;
	}

	public void setIdProposal(long idProposal) {
		this.idProposal = idProposal;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getEtapa() {
		return etapa;
	}

	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getEstadoId() {
		return estadoId;
	}

	public void setEstadoId(String estadoId) {
		this.estadoId = estadoId;
	}

	public String getEscenarioActual() {
		return escenarioActual;
	}

	public void setEscenarioActual(String escenarioActual) {
		this.escenarioActual = escenarioActual;
	}

	public String getUsuarioIdCreacion() {
		return usuarioIdCreacion;
	}

	public void setUsuarioIdCreacion(String usuarioIdCreacion) {
		this.usuarioIdCreacion = usuarioIdCreacion;
	}

	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}

	public String getBancoId() {
		return bancoId;
	}

	public void setBancoId(String bancoId) {
		this.bancoId = bancoId;
	}

	public String getSucursalID() {
		return sucursalID;
	}

	public void setSucursalID(String sucursalID) {
		this.sucursalID = sucursalID;
	}

	public long getCantidadParticipantes() {
		return cantidadParticipantes;
	}

	public void setCantidadParticipantes(long cantidadParticipantes) {
		this.cantidadParticipantes = cantidadParticipantes;
	}

	public long getCantidadProductos() {
		return cantidadProductos;
	}

	public void setCantidadProductos(long cantidadProductos) {
		this.cantidadProductos = cantidadProductos;
	}

	public long getCantidadGarantia() {
		return cantidadGarantia;
	}

	public void setCantidadGarantia(long cantidadGarantia) {
		this.cantidadGarantia = cantidadGarantia;
	}

	public int getInicialId() {
		return inicialId;
	}

	public void setInicialId(int inicialId) {
		this.inicialId = inicialId;
	}

	public double getMontoRequerido() {
		return montoRequerido;
	}

	public void setMontoRequerido(double montoRequerido) {
		this.montoRequerido = montoRequerido;
	}

	public String getMonedaRequerida() {
		return monedaRequerida;
	}

	public void setMonedaRequerida(String monedaRequerida) {
		this.monedaRequerida = monedaRequerida;
	}

	public double getMontoAprobado() {
		return montoAprobado;
	}

	public void setMontoAprobado(double montoAprobado) {
		this.montoAprobado = montoAprobado;
	}

	public String getMonedaAprobado() {
		return monedaAprobado;
	}

	public void setMonedaAprobado(String monedaAprobado) {
		this.monedaAprobado = monedaAprobado;
	}

	public double getMontoDesembolsado() {
		return montoDesembolsado;
	}

	public void setMontoDesembolsado(double montoDesembolsado) {
		this.montoDesembolsado = montoDesembolsado;
	}

	public String getMonedaDesembolsado() {
		return monedaDesembolsado;
	}

	public void setMonedaDesembolsado(String monedaDesembolsado) {
		this.monedaDesembolsado = monedaDesembolsado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bancoId == null) ? 0 : bancoId.hashCode());
		result = prime * result + ((canal == null) ? 0 : canal.hashCode());
		result = prime * result + (int) (cantidadGarantia ^ (cantidadGarantia >>> 32));
		result = prime * result + (int) (cantidadParticipantes ^ (cantidadParticipantes >>> 32));
		result = prime * result + (int) (cantidadProductos ^ (cantidadProductos >>> 32));
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((escenarioActual == null) ? 0 : escenarioActual.hashCode());
		result = prime * result + ((estadoId == null) ? 0 : estadoId.hashCode());
		result = prime * result + ((etapa == null) ? 0 : etapa.hashCode());
		result = prime * result + ((fechaCreacion == null) ? 0 : fechaCreacion.hashCode());
		result = prime * result + (int) (idProposal ^ (idProposal >>> 32));
		result = prime * result + inicialId;
		result = prime * result + ((monedaAprobado == null) ? 0 : monedaAprobado.hashCode());
		result = prime * result + ((monedaDesembolsado == null) ? 0 : monedaDesembolsado.hashCode());
		result = prime * result + ((monedaRequerida == null) ? 0 : monedaRequerida.hashCode());
		long temp;
		temp = Double.doubleToLongBits(montoAprobado);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(montoDesembolsado);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(montoRequerido);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((sucursalID == null) ? 0 : sucursalID.hashCode());
		result = prime * result + ((usuarioIdCreacion == null) ? 0 : usuarioIdCreacion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProposalDTO other = (ProposalDTO) obj;
		if (bancoId == null) {
			if (other.bancoId != null)
				return false;
		} else if (!bancoId.equals(other.bancoId))
			return false;
		if (canal == null) {
			if (other.canal != null)
				return false;
		} else if (!canal.equals(other.canal))
			return false;
		if (cantidadGarantia != other.cantidadGarantia)
			return false;
		if (cantidadParticipantes != other.cantidadParticipantes)
			return false;
		if (cantidadProductos != other.cantidadProductos)
			return false;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (escenarioActual == null) {
			if (other.escenarioActual != null)
				return false;
		} else if (!escenarioActual.equals(other.escenarioActual))
			return false;
		if (estadoId == null) {
			if (other.estadoId != null)
				return false;
		} else if (!estadoId.equals(other.estadoId))
			return false;
		if (etapa == null) {
			if (other.etapa != null)
				return false;
		} else if (!etapa.equals(other.etapa))
			return false;
		if (fechaCreacion == null) {
			if (other.fechaCreacion != null)
				return false;
		} else if (!fechaCreacion.equals(other.fechaCreacion))
			return false;
		if (idProposal != other.idProposal)
			return false;
		if (inicialId != other.inicialId)
			return false;
		if (monedaAprobado == null) {
			if (other.monedaAprobado != null)
				return false;
		} else if (!monedaAprobado.equals(other.monedaAprobado))
			return false;
		if (monedaDesembolsado == null) {
			if (other.monedaDesembolsado != null)
				return false;
		} else if (!monedaDesembolsado.equals(other.monedaDesembolsado))
			return false;
		if (monedaRequerida == null) {
			if (other.monedaRequerida != null)
				return false;
		} else if (!monedaRequerida.equals(other.monedaRequerida))
			return false;
		if (Double.doubleToLongBits(montoAprobado) != Double.doubleToLongBits(other.montoAprobado))
			return false;
		if (Double.doubleToLongBits(montoDesembolsado) != Double.doubleToLongBits(other.montoDesembolsado))
			return false;
		if (Double.doubleToLongBits(montoRequerido) != Double.doubleToLongBits(other.montoRequerido))
			return false;
		if (sucursalID == null) {
			if (other.sucursalID != null)
				return false;
		} else if (!sucursalID.equals(other.sucursalID))
			return false;
		if (usuarioIdCreacion == null) {
			if (other.usuarioIdCreacion != null)
				return false;
		} else if (!usuarioIdCreacion.equals(other.usuarioIdCreacion))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProposalDTO [idCliente=" + idProposal + ", codigo=" + codigo + ", etapa=" + etapa + ", fechaCreacion="
				+ fechaCreacion + ", estadoId=" + estadoId + ", escenarioActual=" + escenarioActual
				+ ", usuarioIdCreacion=" + usuarioIdCreacion + ", canal=" + canal + ", bancoId=" + bancoId
				+ ", sucursalID=" + sucursalID + ", cantidadParticipantes=" + cantidadParticipantes
				+ ", cantidadProductos=" + cantidadProductos + ", cantidadGarantia=" + cantidadGarantia + ", inicialId="
				+ inicialId + ", montoRequerido=" + montoRequerido + ", monedaRequerida=" + monedaRequerida
				+ ", montoAprobado=" + montoAprobado + ", monedaAprobado=" + monedaAprobado + ", montoDesembolsado="
				+ montoDesembolsado + ", monedaDesembolsado=" + monedaDesembolsado + "]";
	}
}
