package com.bbva.pcpy.mock;

import java.util.List;

import com.bbva.pcpy.dto.card.CardDTO;
import com.bbva.pcpy.dto.proposal.ProposalDTO;
import com.bbva.pcpy.lib.r001.PCPYR001;

public class PCPYR001Mock implements PCPYR001 {

	// Anadir todos los execute que tenga la interfaz de la libreria
	//
	// @Override
	// public valor_de_retorno execute...(Parametro1 param1,...) {
	//
	// ...
	//
	// }

	// Metodo execute generado por defecto
	
	@Override
	public ProposalDTO executeGetProposal(String idProposal) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProposalDTO> executeGetAllProposal() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void executeInsertProposal(ProposalDTO dto) {
		// TODO Auto-generated method stub

	}

	@Override
	public void executeUpdateProposal(long idProposal, ProposalDTO dto) {
		// TODO Auto-generated method stub

	}

	@Override
	public void executeDeleteProposal(long idProposal) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<CardDTO> executeGetAllCard() {
		// TODO Auto-generated method stub
		return null;
	}

}
