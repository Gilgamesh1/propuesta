package com.bbva.pcpy;

import com.bbva.pcpy.dto.proposal.ProposalDTO;

import com.bbva.elara.transaction.AbstractTransaction;

public abstract class AbstractPCPYT00101PETransaction extends AbstractTransaction {

	public AbstractPCPYT00101PETransaction(){
	}
	/**
	 * Return value for input parameter idProposal
	 */
	protected String getIdproposal()
	{
		return (String)getParameter("idProposal");
	}
	
	

	
	/**
	 * Set value for output parameter status
	 */
	protected void setStatus(final String field)
	{
		this.addParameter("status", field);
	}
	

	/**
	 * Set value for output parameter EntityOut
	 */
	protected void setEntityout(final ProposalDTO field){
		this.addParameter("EntityOut", field);
	}			
	
}
