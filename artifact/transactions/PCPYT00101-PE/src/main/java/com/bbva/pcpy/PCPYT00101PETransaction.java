package com.bbva.pcpy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.pcpy.dto.proposal.ProposalDTO;
import com.bbva.pcpy.lib.r001.PCPYR001;

/**
 * Retornar una propuesta Implementacion de logica de negocio.
 * 
 * @author user
 *
 */
public class PCPYT00101PETransaction extends AbstractPCPYT00101PETransaction {

	private static final Logger LOGGER = LoggerFactory.getLogger(PCPYT00101PETransaction.class);

	@Override
	public void execute() {
		PCPYR001 pcpyR001 = (PCPYR001) getServiceLibrary(PCPYR001.class);
		LOGGER.info("Iniciando TX1");
		if (this.getIdproposal() != null && !this.getIdproposal().isEmpty()) {
			LOGGER.info("El valor de idProposal {}", this.getIdproposal());
			ProposalDTO proposalDTO = pcpyR001.executeGetProposal(this.getIdproposal());
			LOGGER.info("ProposalDTO {}", proposalDTO);
			this.setEntityout(proposalDTO);
		}
		this.setStatus("Cantidad de valores devueltos= " + 1);
	}

}
