package com.bbva.pcpy;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.pcpy.dto.proposal.ProposalDTO;
import com.bbva.pcpy.lib.r001.PCPYR001;

/**
 * Actualizacion de propuesta Implementacion de logica de negocio.
 * 
 * @author user
 *
 */
public class PCPYT00501PETransaction extends AbstractPCPYT00501PETransaction {

	private static final Logger LOGGER = LoggerFactory.getLogger(PCPYT00501PETransaction.class);

	@Override
	public void execute() {
		PCPYR001 pcpyR001 = (PCPYR001) getServiceLibrary(PCPYR001.class);
		LOGGER.info("Iniciando TX5");
		LOGGER.info("idProposal {}", this.getIdproposal());
		LOGGER.info("codigo {}", this.getCodigo());
		LOGGER.info("getCanal {}", this.getCanal());
		LOGGER.info("getBancoid {}", this.getBancoid());
		LOGGER.info("getEscenarioactual {}", this.getEscenarioactual());
		LOGGER.info("getEstadoid {}", this.getEstadoid());
		LOGGER.info("getEtapa {}", this.getEtapa());
		LOGGER.info("getFechacreacion {}", this.getFechacreacion());
		LOGGER.info("getSucursalid {}", this.getSucursalid());
		LOGGER.info("getUsuarioidcreacion {}", this.getUsuarioidcreacion());
		if (this.getIdproposal() != null) {
			ProposalDTO dto = new ProposalDTO();
			dto.setCanal(this.getCanal());
			dto.setBancoId(this.getBancoid());
			dto.setCantidadGarantia(Integer.valueOf(this.getCantidadgarantia()));
			dto.setCantidadParticipantes(Integer.valueOf(this.getCantidadparticipantes()));
			dto.setCantidadProductos(Integer.valueOf(this.getCantidadproductos()));
			dto.setCodigo(this.getCodigo());
			dto.setEscenarioActual(this.getEscenarioactual());
			dto.setEstadoId(this.getEstadoid());
			dto.setEtapa(this.getEtapa());
			SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
			try {
				dto.setFechaCreacion(sdf.parse(this.getFechacreacion()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			dto.setInicialId(Integer.valueOf(this.getInicialid()));
			dto.setMonedaAprobado(this.getMonedaaprobado());
			dto.setMontoAprobado(Double.valueOf(this.getMontoaprobado()));
			dto.setMonedaDesembolsado(this.getMonedadesembolsado());
			dto.setMontoDesembolsado(Double.valueOf(this.getMontodesembolsado()));
			dto.setMonedaRequerida(this.getMonedarequerida());
			dto.setMontoRequerido(Double.valueOf(this.getMontorequerido()));
			dto.setSucursalID(this.getSucursalid());
			dto.setUsuarioIdCreacion(this.getUsuarioidcreacion());
			pcpyR001.executeUpdateProposal(Long.valueOf(this.getIdproposal()), dto);
		}
		this.setEstado("OK");
	}

}
