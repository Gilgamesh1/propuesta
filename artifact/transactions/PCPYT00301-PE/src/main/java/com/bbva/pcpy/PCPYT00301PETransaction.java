package com.bbva.pcpy;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.pcpy.dto.card.CardDTO;
import com.bbva.pcpy.lib.r001.PCPYR001;

/**
 * Obtener todos los Card Implementacion de logica de negocio.
 * 
 * @author user
 *
 */
public class PCPYT00301PETransaction extends AbstractPCPYT00301PETransaction {

	private static final Logger LOGGER = LoggerFactory.getLogger(PCPYT00301PETransaction.class);

	@Override
	public void execute() {
		PCPYR001 pcpyR001 = (PCPYR001) getServiceLibrary(PCPYR001.class);
		LOGGER.info("Iniciando Tx3 para recuperar Card");
		if (this.getIn() != null) {
			List<CardDTO> cardDtoList = pcpyR001.executeGetAllCard();
			LOGGER.info("Los datos recibidos son: {}", cardDtoList);
			this.setEntityoutlist(cardDtoList);
		} else {
			this.setEntityoutlist(null);
		}
	}

}
