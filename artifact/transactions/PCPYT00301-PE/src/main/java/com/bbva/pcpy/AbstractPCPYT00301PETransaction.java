package com.bbva.pcpy;

import java.util.List;
import com.bbva.pcpy.dto.card.CardDTO;

import com.bbva.elara.transaction.AbstractTransaction;

public abstract class AbstractPCPYT00301PETransaction extends AbstractTransaction {

	public AbstractPCPYT00301PETransaction(){
	}
	/**
	 * Return value for input parameter in
	 */
	protected String getIn()
	{
		return (String)getParameter("in");
	}
	
	

	
	

	/**
	 * Set value for output parameter EntityOutList
	 */
	protected void setEntityoutlist(final List<CardDTO> field){
		this.addParameter("EntityOutList", field);
	}			
	
}
