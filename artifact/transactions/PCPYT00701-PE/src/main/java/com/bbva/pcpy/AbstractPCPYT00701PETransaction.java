package com.bbva.pcpy;


import com.bbva.elara.transaction.AbstractTransaction;

public abstract class AbstractPCPYT00701PETransaction extends AbstractTransaction {

	public AbstractPCPYT00701PETransaction(){
	}
	/**
	 * Return value for input parameter nombre
	 */
	protected String getNombre()
	{
		return (String)getParameter("nombre");
	}
	/**
	 * Return value for input parameter edad
	 */
	protected Long getEdad()
	{
		return (Long)getParameter("edad");
	}
	
	

	
	/**
	 * Set value for output parameter resultado
	 */
	protected void setResultado(final String field)
	{
		this.addParameter("resultado", field);
	}
	

}
