package com.bbva.pcpy;

import com.bbva.pcpy.lib.r001.PCPYR001;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.elara.domain.transaction.Severity;
import com.bbva.elara.domain.transaction.response.HttpResponseCode;
import com.bbva.pcpy.dto.proposal.ProposalDTO;

/**
 * ObjectoDTO Implementacion de logica de negocio.
 * 
 * @author user
 *
 */
public class PCPYT00801PETransaction extends AbstractPCPYT00801PETransaction {

	private static final Logger LOGGER = LoggerFactory.getLogger(PCPYT00801PETransaction.class);

	@Override
	public void execute() {
		PCPYR001 pcpyR001 = (PCPYR001) getServiceLibrary(PCPYR001.class);
		ProposalDTO proposalDTO = this.getEntityin();
		LOGGER.info("Proposal {}", proposalDTO);
		if (proposalDTO != null && proposalDTO.getCodigo().equals("001")) {
			this.setStatus("OK");
			this.setSeverity(Severity.OK);
			this.setHttpResponseCode(HttpResponseCode.HTTP_CODE_202);
		} else {
			this.setStatus("Error");
			this.setSeverity(Severity.WARN);
			this.setHttpResponseCode(HttpResponseCode.HTTP_CODE_400);
			// this.addAdvice("D001");
		}
	}

}
