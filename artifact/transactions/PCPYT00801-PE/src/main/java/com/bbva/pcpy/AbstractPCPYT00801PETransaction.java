package com.bbva.pcpy;

import com.bbva.pcpy.dto.proposal.ProposalDTO;

import com.bbva.elara.transaction.AbstractTransaction;

public abstract class AbstractPCPYT00801PETransaction extends AbstractTransaction {

	public AbstractPCPYT00801PETransaction(){
	}
	
	

	/**
	 * Return value for input parameter EntityIn
	 */
	protected ProposalDTO getEntityin(){
		return (ProposalDTO)getParameter("EntityIn");
	}
	
	
	/**
	 * Set value for output parameter status
	 */
	protected void setStatus(final String field)
	{
		this.addParameter("status", field);
	}
	

}
