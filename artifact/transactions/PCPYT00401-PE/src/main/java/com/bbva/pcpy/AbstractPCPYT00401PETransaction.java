package com.bbva.pcpy;


import com.bbva.elara.transaction.AbstractTransaction;

public abstract class AbstractPCPYT00401PETransaction extends AbstractTransaction {

	public AbstractPCPYT00401PETransaction(){
	}
	/**
	 * Return value for input parameter codigo
	 */
	protected String getCodigo()
	{
		return (String)getParameter("codigo");
	}
	/**
	 * Return value for input parameter etapa
	 */
	protected String getEtapa()
	{
		return (String)getParameter("etapa");
	}
	/**
	 * Return value for input parameter fechaCreacion
	 */
	protected String getFechacreacion()
	{
		return (String)getParameter("fechaCreacion");
	}
	/**
	 * Return value for input parameter estadoId
	 */
	protected String getEstadoid()
	{
		return (String)getParameter("estadoId");
	}
	/**
	 * Return value for input parameter escenarioActual
	 */
	protected String getEscenarioactual()
	{
		return (String)getParameter("escenarioActual");
	}
	/**
	 * Return value for input parameter usuarioIdCreacion
	 */
	protected String getUsuarioidcreacion()
	{
		return (String)getParameter("usuarioIdCreacion");
	}
	/**
	 * Return value for input parameter canal
	 */
	protected String getCanal()
	{
		return (String)getParameter("canal");
	}
	/**
	 * Return value for input parameter bancoId
	 */
	protected String getBancoid()
	{
		return (String)getParameter("bancoId");
	}
	/**
	 * Return value for input parameter sucursalID
	 */
	protected String getSucursalid()
	{
		return (String)getParameter("sucursalID");
	}
	/**
	 * Return value for input parameter cantidadParticipantes
	 */
	protected String getCantidadparticipantes()
	{
		return (String)getParameter("cantidadParticipantes");
	}
	/**
	 * Return value for input parameter cantidadProductos
	 */
	protected String getCantidadproductos()
	{
		return (String)getParameter("cantidadProductos");
	}
	/**
	 * Return value for input parameter cantidadGarantia
	 */
	protected String getCantidadgarantia()
	{
		return (String)getParameter("cantidadGarantia");
	}
	/**
	 * Return value for input parameter inicialId
	 */
	protected String getInicialid()
	{
		return (String)getParameter("inicialId");
	}
	/**
	 * Return value for input parameter montoRequerido
	 */
	protected String getMontorequerido()
	{
		return (String)getParameter("montoRequerido");
	}
	/**
	 * Return value for input parameter monedaRequerida
	 */
	protected String getMonedarequerida()
	{
		return (String)getParameter("monedaRequerida");
	}
	/**
	 * Return value for input parameter montoAprobado
	 */
	protected String getMontoaprobado()
	{
		return (String)getParameter("montoAprobado");
	}
	/**
	 * Return value for input parameter monedaAprobado
	 */
	protected String getMonedaaprobado()
	{
		return (String)getParameter("monedaAprobado");
	}
	/**
	 * Return value for input parameter montoDesembolsado
	 */
	protected String getMontodesembolsado()
	{
		return (String)getParameter("montoDesembolsado");
	}
	/**
	 * Return value for input parameter monedaDesembolsado
	 */
	protected String getMonedadesembolsado()
	{
		return (String)getParameter("monedaDesembolsado");
	}
	
	

	
	/**
	 * Set value for output parameter status
	 */
	protected void setStatus(final String field)
	{
		this.addParameter("status", field);
	}
	

}
