package com.bbva.pcpy;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.pcpy.dto.proposal.ProposalDTO;
import com.bbva.pcpy.lib.r001.PCPYR001;

/**
 * obtener todas las propuestas Implementacion de logica de negocio.
 * 
 * @author user
 *
 */
public class PCPYT00201PETransaction extends AbstractPCPYT00201PETransaction {

	private static final Logger LOGGER = LoggerFactory.getLogger(PCPYT00201PETransaction.class);

	@Override
	public void execute() {
		PCPYR001 pcpyR001 = (PCPYR001) getServiceLibrary(PCPYR001.class);
		LOGGER.info("Iniciando TX2");
		List<ProposalDTO> proposalDTOs = pcpyR001.executeGetAllProposal();
		LOGGER.info("Lista de Propuestas {}", proposalDTOs);
		this.setResultado("resultado");
		this.setEntiytoutlist(proposalDTOs);
	}

}
