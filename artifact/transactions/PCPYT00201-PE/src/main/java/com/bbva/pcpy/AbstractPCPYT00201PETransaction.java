package com.bbva.pcpy;

import java.util.List;
import com.bbva.pcpy.dto.proposal.ProposalDTO;

import com.bbva.elara.transaction.AbstractTransaction;

public abstract class AbstractPCPYT00201PETransaction extends AbstractTransaction {

	public AbstractPCPYT00201PETransaction(){
	}
	
	

	
	/**
	 * Set value for output parameter resultado
	 */
	protected void setResultado(final String field)
	{
		this.addParameter("resultado", field);
	}
	

	/**
	 * Set value for output parameter EntiytOutList
	 */
	protected void setEntiytoutlist(final List<ProposalDTO> field){
		this.addParameter("EntiytOutList", field);
	}			
	
}
