package com.bbva.pcpy;


import com.bbva.elara.transaction.AbstractTransaction;

public abstract class AbstractPCPYT00601PETransaction extends AbstractTransaction {

	public AbstractPCPYT00601PETransaction(){
	}
	/**
	 * Return value for input parameter idProposal
	 */
	protected String getIdproposal()
	{
		return (String)getParameter("idProposal");
	}
	
	

	
	/**
	 * Set value for output parameter estado
	 */
	protected void setEstado(final String field)
	{
		this.addParameter("estado", field);
	}
	

}
