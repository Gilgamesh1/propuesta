package com.bbva.pcpy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.pcpy.lib.r001.PCPYR001;

/**
 * borrado de propuesta Implementacion de logica de negocio.
 * 
 * @author user
 *
 */
public class PCPYT00601PETransaction extends AbstractPCPYT00601PETransaction {
	private static final Logger LOGGER = LoggerFactory.getLogger(PCPYT00601PETransaction.class);

	@Override
	public void execute() {
		PCPYR001 pcpyR001 = (PCPYR001) getServiceLibrary(PCPYR001.class);
		LOGGER.info("Iniciando TX6");
		if (this.getIdproposal() != null) {
			long idProposal = Long.valueOf(this.getIdproposal());
			LOGGER.info("El valor de idProposal: {}", idProposal);
			pcpyR001.executeDeleteProposal(idProposal);
		}
		this.setEstado("OK");
	}

}
