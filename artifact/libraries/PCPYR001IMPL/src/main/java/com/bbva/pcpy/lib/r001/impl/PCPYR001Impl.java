package com.bbva.pcpy.lib.r001.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.elara.exception.runtime.hierarchy.JdbcRuntimeException;
import com.bbva.pcpy.dto.card.CardDTO;
import com.bbva.pcpy.dto.proposal.ProposalDTO;
import com.bbva.pcpy.lib.r001.PCPYR001;

public class PCPYR001Impl extends PCPYR001Abstract {

    private static final Logger LOGGER = LoggerFactory.getLogger(PCPYR001.class);

    @Override
    public ProposalDTO executeGetProposal(String idProposal) {
        LOGGER.info("INICIO PCPYR001Impl - executeGetProposal");
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("idProposal", idProposal);
        ProposalDTO proposalDTO = null;
        try {
            Map<String, Object> proposalMap = this.jdbcUtils.queryForMap("sql.pcpy.proposal.getone", map);
            BigDecimal bigDecimal = null;
            if (!proposalMap.isEmpty()) {
                proposalDTO = new ProposalDTO();
                proposalDTO.setCodigo((String) proposalMap.get("CODIGO"));
                proposalDTO.setEtapa((String) proposalMap.get("ETAPA"));
                proposalDTO.setFechaCreacion((Date) proposalMap.get("FECHA_CREACION"));
                proposalDTO.setEstadoId((String) proposalMap.get("ESTADO_ID"));
                proposalDTO.setEscenarioActual((String) proposalMap.get("ESCENARIO_ACTUAL"));
                proposalDTO.setUsuarioIdCreacion((String) proposalMap.get("USUARIO_ID_CREACION"));
                proposalDTO.setCanal((String) proposalMap.get("CANAL"));
                proposalDTO.setBancoId((String) proposalMap.get("BANCO_ID"));
                proposalDTO.setSucursalID((String) proposalMap.get("SUCURSAL_ID"));
                bigDecimal = (BigDecimal) proposalMap.get("CANTIDAD_PARTICIPANTES");
                proposalDTO.setCantidadParticipantes(bigDecimal.intValueExact());
                bigDecimal = (BigDecimal) proposalMap.get("CANTIDAD_PRODUCTOS");
                proposalDTO.setCantidadProductos(bigDecimal.intValueExact());
                bigDecimal = (BigDecimal) proposalMap.get("CANTIDAD_GARANTIA");
                proposalDTO.setCantidadGarantia(bigDecimal.intValueExact());
                bigDecimal = (BigDecimal) proposalMap.get("INICIAL_ID");
                proposalDTO.setInicialId(bigDecimal.intValueExact());
                bigDecimal = (BigDecimal) proposalMap.get("MONTO_REQUERIDO");
                proposalDTO.setMontoRequerido(bigDecimal.doubleValue());
                proposalDTO.setMonedaRequerida((String) proposalMap.get("MONEDA_REQUERIDA"));
                bigDecimal = (BigDecimal) proposalMap.get("MONTO_APROBADO");
                proposalDTO.setMontoAprobado(bigDecimal.doubleValue());
                proposalDTO.setMonedaAprobado((String) proposalMap.get("MONEDA_APROBADO"));
                bigDecimal = (BigDecimal) proposalMap.get("MONTO_DESEMBOLSADO");
                proposalDTO.setMontoDesembolsado(bigDecimal.doubleValue());
                proposalDTO.setMonedaDesembolsado((String) proposalMap.get("MONEDA_DESEMBOLDADO"));
            }
            LOGGER.info("PROBANDO SECUENCIADOR ......");
            Map<String, Object> sequencialMap = this.jdbcUtils.queryForMap("sql.pcpy.next");
            bigDecimal = (BigDecimal) sequencialMap.get("next_id");
            long secuenciador = bigDecimal.longValue();
            LOGGER.info("valor del SECUENCIADOR {}", sequencialMap);
        } catch (JdbcRuntimeException e) {
            LOGGER.error("Error al recuperar propuesta", e);
        }
        return proposalDTO;
    }

    @Override
    public List<ProposalDTO> executeGetAllProposal() {
        LOGGER.info("INICIO PCPYR001Impl - executeGetAllProposal");
        List<Map<String, Object>> proposalList = this.jdbcUtils.queryForList("sql.pcpy.proposal.getall");
        List<ProposalDTO> proposalListResult = null;
        try {
            if (!proposalList.isEmpty()) {
                LOGGER.info("todos las propuestas: {}", proposalList);
                proposalListResult = new ArrayList<ProposalDTO>();
                for (Map<String, Object> proposalMap : proposalList) {
                    ProposalDTO proposalDTO = new ProposalDTO();
                    proposalDTO.setCodigo((String) proposalMap.get("CODIGO"));
                    proposalDTO.setEtapa((String) proposalMap.get("ETAPA"));
                    proposalDTO.setFechaCreacion((Date) proposalMap.get("FECHA_CREACION"));
                    proposalDTO.setEstadoId((String) proposalMap.get("ESTADO_ID"));
                    proposalDTO.setEscenarioActual((String) proposalMap.get("ESCENARIO_ACTUAL"));
                    proposalDTO.setUsuarioIdCreacion((String) proposalMap.get("USUARIO_ID_CREACION"));
                    proposalDTO.setCanal((String) proposalMap.get("CANAL"));
                    proposalDTO.setBancoId((String) proposalMap.get("BANCO_ID"));
                    proposalDTO.setSucursalID((String) proposalMap.get("SUCURSAL_ID"));
                    BigDecimal bigDecimal = (BigDecimal) proposalMap.get("CANTIDAD_PARTICIPANTES");
                    proposalDTO.setCantidadParticipantes(bigDecimal.intValueExact());
                    bigDecimal = (BigDecimal) proposalMap.get("CANTIDAD_PRODUCTOS");
                    proposalDTO.setCantidadProductos(bigDecimal.intValueExact());
                    bigDecimal = (BigDecimal) proposalMap.get("CANTIDAD_GARANTIA");
                    proposalDTO.setCantidadGarantia(bigDecimal.intValueExact());
                    bigDecimal = (BigDecimal) proposalMap.get("INICIAL_ID");
                    proposalDTO.setInicialId(bigDecimal.intValueExact());
                    bigDecimal = (BigDecimal) proposalMap.get("MONTO_REQUERIDO");
                    proposalDTO.setMontoRequerido(bigDecimal.doubleValue());
                    proposalDTO.setMonedaRequerida((String) proposalMap.get("MONEDA_REQUERIDA"));
                    bigDecimal = (BigDecimal) proposalMap.get("MONTO_APROBADO");
                    proposalDTO.setMontoAprobado(bigDecimal.doubleValue());
                    proposalDTO.setMonedaAprobado((String) proposalMap.get("MONEDA_APROBADO"));
                    bigDecimal = (BigDecimal) proposalMap.get("MONTO_DESEMBOLSADO");
                    proposalDTO.setMontoDesembolsado(bigDecimal.doubleValue());
                    proposalDTO.setMonedaDesembolsado((String) proposalMap.get("MONEDA_DESEMBOLDADO"));
                    proposalListResult.add(proposalDTO);
                }
            }
        } catch (JdbcRuntimeException e) {
            LOGGER.error("Error al recuperar propuesta", e);
        }
        LOGGER.info("Tamaño proposalListResult: {}", proposalListResult.size());
        return proposalListResult;
    }

    @Override
    public void executeInsertProposal(ProposalDTO dto) {
        LOGGER.info("INICIO PCPYR001Impl - executeInsertProposal");
        LOGGER.info("Valores recibidos {}", dto);
        try {
            int result = this.jdbcUtils.update("sql.pcpy.proposal.insert", dto.getCodigo(), dto.getEtapa(),
                    dto.getFechaCreacion(), dto.getEstadoId(), dto.getEscenarioActual(), dto.getUsuarioIdCreacion(),
                    dto.getCanal(), dto.getBancoId(), dto.getSucursalID(), dto.getCantidadParticipantes(),
                    dto.getCantidadProductos(), dto.getCantidadGarantia(), dto.getInicialId(), dto.getMontoRequerido(),
                    dto.getMonedaRequerida(), dto.getMontoAprobado(), dto.getMonedaAprobado(),
                    dto.getMontoDesembolsado(), dto.getMonedaDesembolsado());
            LOGGER.info("Resultado de la operacion {}", result);
        } catch (JdbcRuntimeException e) {
            LOGGER.error("Error durante la insercion de la propuesta", e);
        }
    }

    @Override
    public void executeUpdateProposal(long idProposal, ProposalDTO dto) {
        LOGGER.info("INICIO PCPYR001Impl - executeUpdateProposal");
        try {
            this.jdbcUtils.update("sql.pcpy.proposal.update", dto.getCodigo(), dto.getEtapa(), dto.getFechaCreacion(),
                    dto.getEstadoId(), dto.getEscenarioActual(), dto.getUsuarioIdCreacion(), dto.getCanal(),
                    dto.getBancoId(), dto.getSucursalID(), dto.getCantidadParticipantes(), dto.getCantidadProductos(),
                    dto.getCantidadGarantia(), dto.getInicialId(), dto.getMontoRequerido(), dto.getMonedaRequerida(),
                    dto.getMontoAprobado(), dto.getMonedaAprobado(), dto.getMontoDesembolsado(),
                    dto.getMonedaDesembolsado(), idProposal);
        } catch (JdbcRuntimeException e) {
            LOGGER.error("Error durante la actualizacion de la propuesta", e);
        }
    }

    @Override
    public void executeDeleteProposal(long idProposal) {
        LOGGER.info("INICIO PCPYR001Impl - executeDeleteProposal");
        try {
            this.jdbcUtils.update("sql.pcpy.proposal.delete", idProposal);
        } catch (JdbcRuntimeException e) {
            LOGGER.error("Error durante la actualizacion de la propuesta", e);
        }
    }

    @Override
    public List<CardDTO> executeGetAllCard() {
        LOGGER.info("INICIO PCPYR001Impl - executeGetAllCard");
        List<Map<String, Object>> cardDbList = this.jdbcUtils.queryForList("sql.pcpy.card.getall");
        LOGGER.info("cardDbList: {}", cardDbList);
        List<CardDTO> cardDtoList = new ArrayList<CardDTO>();
        for (Map<String, Object> map : cardDbList) {
            CardDTO card = new CardDTO();
            card.setId((String) map.get("ID"));
            card.setNombre((String) map.get("NAME"));
            card.setApellido((String) map.get("NUM"));
            cardDtoList.add(card);
        }
        /*
         * CardDTO card = new CardDTO(); card.setId("ID");
         * card.setNombre("NAME"); card.setApellido("NUM");
         * cardDtoList.add(card);
         */
        LOGGER.info("Tamaño cardDtoList: {}", cardDtoList.size());
        return cardDtoList;
    }
}
