package com.bbva.pcpy.lib.r001;

import java.util.List;

import com.bbva.pcpy.dto.card.CardDTO;
import com.bbva.pcpy.dto.proposal.ProposalDTO;

public interface PCPYR001 {

	ProposalDTO executeGetProposal(String idProposal);

	List<ProposalDTO> executeGetAllProposal();

	void executeInsertProposal(ProposalDTO dto);

	void executeUpdateProposal(long idProposal, ProposalDTO dto);

	void executeDeleteProposal(long idProposal);

	List<CardDTO> executeGetAllCard();

}
